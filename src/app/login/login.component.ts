import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { NavService, LocalService, LoginService } from '../core';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from '../core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  isLoggedIn: boolean = false;

  constructor(private fb: FormBuilder,
              protected router: Router, 
              protected navService: NavService,
              private localService: LocalService,
              private notifications: NotificationsService,
              private loginService: LoginService) {

  }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      userName: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  onSubmit(): void {
    const data = {
      userName: this.loginForm.value.userName,
      password: this.loginForm.value.password
    };
    this.loginService.login(data)
      .subscribe((data: User) => {
        if(data) {
          this.localService.setUser(data);
          this.localService.setLoggedIn(true);
          this.navService.emitLoginNavChangeEvent(true);
          this.router.navigate(['/dashboard']);
        } else {
          const errorMsg = 'Please Enter a Valid username and password';
          this.notifications.error('Error', errorMsg, this.localService.getNotificationsConfig({}));
        }
      }, (err: Error) => {
        const errorMsg = err ? err : 'Error while Loggin In';
        this.notifications.error('Error', errorMsg, this.localService.getNotificationsConfig({}));
      });
  }

  logout(): void {
    this.localService.setLoggedIn(false);
    this.navService.emitLoginNavChangeEvent(false);
    this.router.navigate(['/login']);
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatTabsModule, MatMenuModule, MatCardModule, MatIconModule,
         MatButtonModule, MatTableModule, MatListModule, MatSidenavModule, 
         MatExpansionModule, MatBadgeModule, MatIconRegistry, MatDialog, MatDialogModule } from '@angular/material';

import { SimpleNotificationsModule } from 'angular2-notifications';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppComponent } from './app.component';
import { SharedModule, HeaderComponent, ConfirmationDialog, MessageDialog } from './shared';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { AppNotificationService, NavService } from './core';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ConfirmationDialog,
    MessageDialog
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    ReactiveFormsModule,
    SimpleNotificationsModule.forRoot(),
    CoreModule,
    SharedModule,
    MatDialogModule,
    MatTabsModule,
    MatMenuModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    MatTableModule,
    MatListModule,
    MatSidenavModule,
    MatExpansionModule,
    MatBadgeModule,
    MatIconModule,
    FontAwesomeModule
  ],
  providers: [
    AppNotificationService,
    NavService,
    MatIconRegistry
  ],
  bootstrap: [AppComponent],
  entryComponents: [MessageDialog, ConfirmationDialog]
})
export class AppModule { }

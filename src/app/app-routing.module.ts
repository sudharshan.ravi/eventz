import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

import { DashboardModule } from './dashboard/dashboard.module';
import { LoginModule } from './login/login.module';
import { EventModule } from './event/event.module';

const appRoutes: Routes = [
  { 
    path: 'dashboard', 
    loadChildren: () => DashboardModule
  },
  {
    path: 'event',
    loadChildren: () => EventModule
  },
  {
    path: 'login',
    loadChildren: () => LoginModule
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {
    // preload all modules; optionally we could
    // implement a custom preloading strategy for just some
    // of the modules (PRs welcome 😉)
    preloadingStrategy: PreloadAllModules,
    enableTracing: true  // <-- debugging purposes only
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {}

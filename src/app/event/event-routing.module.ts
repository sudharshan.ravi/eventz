import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventComponent } from './event.component';
import { EventDetailsComponent } from './event-details/event-details.component';
import { EventsListComponent } from './event-list/event-list.component';
import { EventParticipantsComponent } from './event-participants/event-participants.component';
import { EventRegisterComponent } from './event-register/event-register.component';

const routes: Routes = [
  { path: '', component: EventComponent,
    children: [
      { path: 'my-events', component: EventsListComponent },
      { path: 'add', component: EventDetailsComponent },
      { path: ':eventId', component: EventDetailsComponent },
      { path: ':eventId/users', component: EventParticipantsComponent },
      { path: ':eventId/register', component: EventRegisterComponent }
    ]
  }
];

export const routedComponents = [EventComponent, EventDetailsComponent, EventsListComponent,
                                 EventParticipantsComponent, EventRegisterComponent];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EventRoutingModule { }

import { Component, OnInit, ElementRef, ViewChild} from '@angular/core';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { NotificationsService } from 'angular2-notifications';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { LocalService, EventService, AppEvent, ConfiguredField, FieldType } from '../../core';

@Component({
  templateUrl: './event-register.component.html'
})
export class EventRegisterComponent implements OnInit {
  
  eventForm: FormGroup;
  eventRegisterForm: FormGroup;

  userId: number;
  eventId: string;
  loading = false;
  attributes: Array<ConfiguredField> = [];
  fieldTypes: Array<FieldType> = [
    {type: "text", name: "Text"},
    {type: "textarea", name: "Text Area"}
  ];
  eventDetails: AppEvent;

  constructor(
    private fb: FormBuilder,
    private localService: LocalService,
    private eventService: EventService,
    private notifications: NotificationsService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private location: Location
  ) {
    this.eventId = this.activatedRoute.snapshot.params.eventId;
    this.getEventDetails(this.eventId);
  }

  ngOnInit(): void {
    this.userId = this.localService.getUser().id;
  }

  goToDashboard(): void {
    this.router.navigateByUrl(`/dashboard`);
  }

  createForm(): void {
    let configuredFieldsList: any = {};
    this.eventDetails.configureFields.forEach((item) => {
      configuredFieldsList[item.name] = new FormControl(item.value, item.mandatory ? [Validators.required, Validators.maxLength(500)] : [Validators.maxLength(500)]);
    });
    this.eventRegisterForm = this.fb.group(configuredFieldsList);                         
    this.attributes = this.eventDetails.configureFields;
  }

  getEventDetails(eventId: string): void {
    this.eventService.getEventDetails(eventId).subscribe((data: AppEvent) => {
      if (data) {
        this.eventDetails = data;
        this.createForm();
      }
    }, (err: any) => {
        const errorMsg = err ? err : `Error while fetching Event details`;
        this.notifications.error('Error', errorMsg, this.localService.getNotificationsConfig({}));
      }
    );
  }

  registerToEvent(): void {
    if (this.eventRegisterForm.valid) {
      let eventDetails = Object.assign({}, this.eventDetails);
      let configureFields = JSON.parse(JSON.stringify(this.eventDetails.configureFields));
      configureFields.forEach(item => {
        item.value = this.eventRegisterForm.value[item.name];
      });
      eventDetails.registeredUsers.push({id: this.userId, fieldDetails: configureFields});
      this.loading = true;
      this.eventService.register(this.userId, eventDetails)
        .subscribe((data: Array<AppEvent>) => {
          this.loading = false;
          if (data) {
            const successMsg = `You have successfully Registered to Event: ${eventDetails.name}`;
            this.notifications.success('Success', successMsg, this.localService.getNotificationsConfig({}));
            this.location.back();
          }
        }, (err: Error) => {
            this.loading = false;
            const errorMsg = err ? err : `Error while Registering to Event: ${eventDetails.name}`;
            this.notifications.error('Error', errorMsg, this.localService.getNotificationsConfig({}));
            this.location.back();
        });
    }
  }

  cancel(): void {
    this.location.back();
  }
}

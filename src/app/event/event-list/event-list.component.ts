import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { NotificationsService } from 'angular2-notifications';
import { faTrashAlt, faEdit, faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';

import { EventService, LocalService, AppEvent, 
         convertMinsToHrsMins, ConvertCurrencyToINR } from '../../core';
import { ConfirmationDialog } from '../../shared';

@Component({
  templateUrl: './event-list.component.html'
})
export class EventsListComponent {

  faTrashAlt = faTrashAlt;
  faEdit = faEdit;
  faEye = faEye;
  faEyeSlash = faEyeSlash;
  convertMinsToHrsMins = convertMinsToHrsMins
  ConvertCurrencyToINR = ConvertCurrencyToINR;

  userId: number;
  eventsData = [];
  displayedColumns = ['position', 'name', 'date', 'duration', 'location', 'fees', 'maxParticipants', 'viewUsers', 'edit', 'delete'];
  eventDataSource: any = [];
  loading = false;

  constructor(
    private eventService: EventService,
    private notifications: NotificationsService,
    private localService: LocalService,
    private router: Router,
    public dialog: MatDialog,
  ) {  
    this.userId = this.localService.getUser().id;
    this.getEvents(this.userId);
  }

  getEvents(userId: number) {
    this.loading = true;
    this.eventService.getMyEvents(userId).subscribe((data: Array<AppEvent>) => {
      this.loading = false;
      if (data && data.length) {
        let events = [];
        data.forEach((value, index) => {
          events.push({
            position: index + 1,
            name: value.name,
            date: value.date,
            duration: value.duration,
            location: value.location,
            fees: value.fees,
            maxParticipants: value.maxParticipants,
            registeredUsers: value.registeredUsers.length,
            id: value.id
          });
        });
        this.eventsData = events;
        this.eventDataSource = new MatTableDataSource(this.eventsData);
      }
    }, (err: any) => {
      this.loading = false;
      const errorMsg = err ? err : `Error while fetching my events`;
      this.notifications.error('Error', errorMsg, this.localService.getNotificationsConfig({}));
    });
  }

  deleteEvent(name: string, eventId: string) {
    let dialogRef = this.dialog.open(ConfirmationDialog, {
      disableClose: false
    });

    dialogRef.componentInstance.confirmMessage = `Are you sure you want to delete Event: ${name}`;

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.eventService.removeEvent(eventId).subscribe((data: boolean) => {
          if(data) {
            const successMsg = `Event: ${name} is deleted Successfully!`;
            this.notifications.success('Success', successMsg, this.localService.getNotificationsConfig({}));
            let newEvents = this.eventsData.filter(value => eventId !== value.id);
            newEvents.forEach((event, index) => {
              event.position = index + 1;
            })
            this.eventsData = newEvents;
            this.eventDataSource = new MatTableDataSource(this.eventsData);
          }
         }, (err: Error) => {
            const errorMsg = err ? err : `Error while deleting Event: ${name}`;
            this.notifications.error('Error', errorMsg, this.localService.getNotificationsConfig({}));
        });
      }
      dialogRef = null;
    });
  }

  goToDashboard() {
    this.router.navigateByUrl(`/dashboard`);
  }

  createEvent() {
    this.router.navigateByUrl(`/event/add`);   
  }

  editEvent(eventId: string) {
    this.router.navigateByUrl(`/event/${eventId}`);
  }

  viewUsers(eventId: string) {
    this.router.navigateByUrl(`/event/${eventId}/users`);
  }
}

import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { faEye } from '@fortawesome/free-solid-svg-icons';

import { EventService, LocalService, User, AppEvent } from '../../core';
import { MessageDialog } from '../../shared'

@Component({
  templateUrl: './event-participants.component.html'
})
export class EventParticipantsComponent {

  faEye = faEye;
  eventId: string;
  event: AppEvent;
  usersData = [];
  displayedColumns = ['position', 'userName', 'firstName', 'lastName', 'email', 'configuredFields'];
  userDataSource: any = [];
  loading = false;

  constructor(
    private eventService: EventService,
    private notifications: NotificationsService,
    private localService: LocalService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog,
  ) {  
    this.eventId = this.activatedRoute.snapshot.params.eventId;
    this.getEventDetails(this.eventId);
    this.getRegisteredUsers(this.eventId);
  }

  getEventDetails(eventId: string) {
    this.loading = true;
    this.eventService.getEventDetails(eventId).subscribe((data: AppEvent) => {
      this.loading = false;
      this.event = data;
    }, (err: any) => {
      this.loading = false;
      const errorMsg = err ? err : `Error while fetching Event Details`;
      this.notifications.error('Error', errorMsg, this.localService.getNotificationsConfig({}));
    });
  }

  getRegisteredUsers(eventId: string) {
    this.loading = true;
    this.eventService.getRegisteredUsers(eventId).subscribe((data: Array<User>) => {
      this.loading = false;
      if (data && data.length) {
        let users = [];
        data.forEach((value, index) => {
          users.push({
            position: index + 1,
            userName: value.userName,
            firstName: value.firstName,
            lastName: value.lastName,
            email: value.email,
            user: value
          });
        });
        this.usersData = users;
        this.userDataSource = new MatTableDataSource(this.usersData);
      }
    }, (err: any) => {
      this.loading = false;
      const errorMsg = err ? err : `Error while fetching Registered users`;
      this.notifications.error('Error', errorMsg, this.localService.getNotificationsConfig({}));
    });
  }

  viewConfiguredFields(user: User) {
    let configuredFields = this.event.registeredUsers.filter(value => value.id === user.id)[0];
    let dialogRef = this.dialog.open(MessageDialog, {
      disableClose: false
    });
    dialogRef.componentInstance.messageTitle = `Configured Field Details of ${user.firstName} ${user.lastName}`;
    dialogRef.componentInstance.messages = configuredFields.fieldDetails ? configuredFields.fieldDetails : [];
    dialogRef.afterClosed().subscribe(() => {
      dialogRef = null;
    });
  }

  goToDashboard() {
    this.router.navigateByUrl(`/dashboard`);
  }

  goToMyEvents() {
    this.router.navigateByUrl(`/event/my-events`);
  }
}

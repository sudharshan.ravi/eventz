import { Component, OnInit, ElementRef, ViewChild} from '@angular/core';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MatAutocompleteSelectedEvent, MatChipInputEvent } from '@angular/material';
import { NotificationsService } from 'angular2-notifications';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { faPlus, faTrashAlt } from '@fortawesome/free-solid-svg-icons';

import { LocalService, EventService, AppEvent, ConfiguredField, FieldType } from '../../core';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  templateUrl: './event-details.component.html'
})
export class EventDetailsComponent implements OnInit {
  
  faPlus = faPlus
  faTrashAlt = faTrashAlt;
  eventForm: FormGroup;
  configuredFieldsForm: FormGroup;

  eventId: string;
  isEditMode = false;
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = false;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  filteredTags: Observable<string[]>;
  tags: string[] = [];
  allTags: string[] = [];
  attributes: Array<ConfiguredField> = [];
  fieldTypes: Array<FieldType> = [
    {type: "text", name: "Text"},
    {type: "textarea", name: "Text Area"}
  ];
  eventDetails: AppEvent = {
    id: null,
    name: '',
    description: '',
    date: null,
    duration: null,
    location: '',
    fees: null,
    tags: [],
    maxParticipants: null,
    configureFields: [],
    registeredUsers: [],
    createdBy: null
  };
  eventMinDate = new Date('09/12/2018');
  eventMaxDate = new Date('03/12/2019');

  constructor(
    private fb: FormBuilder,
    private localService: LocalService,
    private eventService: EventService,
    private notifications: NotificationsService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private location: Location
  ) {
    this.eventId = this.activatedRoute.snapshot.params.eventId;
  }

  @ViewChild('tagInput') tagInput: ElementRef<HTMLInputElement>;

  ngOnInit(): void {
    this.getTags();
    this.createForm();
    if (this.eventId && this.eventId !== 'add') {
      this.isEditMode = true;
      this.getEventDetails(this.eventId);
    }
  }

  goToDashboard(): void {
    this.router.navigateByUrl(`/dashboard`);
  }

  createForm(): void {
    this.tags = this.eventDetails.tags;
    this.eventForm = this.fb.group({
      name: new FormControl(this.eventDetails.name, [Validators.required, Validators.minLength(4), Validators.maxLength(50)]),
      description: new FormControl(this.eventDetails.description, [Validators.required, Validators.minLength(4), Validators.maxLength(500)]),
      date: new FormControl(this.eventDetails.date, Validators.required),
      duration: new FormControl(this.eventDetails.duration, [Validators.required, Validators.min(0), Validators.max(7200)]),
      location: new FormControl(this.eventDetails.location, [Validators.required, Validators.minLength(4), Validators.maxLength(50)]),
      fees: new FormControl(this.eventDetails.fees, [Validators.required, Validators.min(0), Validators.max(10000)]),
      tags: new FormControl(this.eventDetails.tags.join(''), [Validators.required, Validators.minLength(4)]),
      maxParticipants: new FormControl(this.eventDetails.maxParticipants, [Validators.required, Validators.min(1), Validators.max(1000000)])
    });
    this.filteredTags = this.eventForm.controls['tags'].valueChanges
                          .pipe(startWith(null),
                                map((tag: string | null) => tag ? this._filter(tag) : this.allTags.slice()));
    if(this.isEditMode) {
      let configuredFieldsList: any = {};
      this.eventDetails.configureFields.forEach((value) => {
        configuredFieldsList[value.name] = this.fb.group({
          id: value.id,
          name: value.name,
          type: value.type,
          mandatory: value.mandatory,
          value: value.value
        });
        this.attributes.push(value);
      });
      this.configuredFieldsForm = this.fb.group(configuredFieldsList);
    } else {
      this.configuredFieldsForm = this.fb.group({});
    }                               
  }

  getTags(): void {
    this.eventService.getTags().subscribe((data: string[]) => {
      this.allTags = data;
    }, (err: Error) => {
      const errorMsg = err ? err : `Error while fetching Tags`;
      this.notifications.error('Error', errorMsg, this.localService.getNotificationsConfig({}));
    });
  }

  getEventDetails(eventId: string): void {
    this.eventService.getEventDetails(eventId).subscribe((data: AppEvent) => {
      if (data) {
        this.eventDetails = data;
        this.createForm();
      }
    }, (err: any) => {
        const errorMsg = err ? err : `Error while fetching Event details`;
        this.notifications.error('Error', errorMsg, this.localService.getNotificationsConfig({}));
      }
    );
  }

  addTag(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    // Add our tags
    if ((value || '').trim()) {
      this.tags.push(value.trim());
    }
    // Reset the input value
    if (input) {
      input.value = '';
    }
    this.eventForm.controls['tags'].setValue(this.tags.join(','));
  }

  removeTag(tag: string): void {
    const index = this.tags.indexOf(tag);
    if (index >= 0) {
      this.tags.splice(index, 1);
      this.eventForm.controls['tags'].setValue(this.tags.join(','));
    }
  }

  selectedTag(event: MatAutocompleteSelectedEvent): void {
    this.tags.push(event.option.viewValue);
    this.tagInput.nativeElement.value = '';
    this.eventForm.controls['tags'].setValue(this.tags.join(','));
  }

  onSubmit(): void {
    if (this.isEditMode) {
      this.updateEventDetails();
    } else {
      this.saveEventDetails();
    }
  }

  saveEventDetails(): void {
    if (this.eventForm.valid) {
      let eventDetails = this.eventForm.value;
      if(typeof eventDetails.tags === 'string') {
        eventDetails.tags = eventDetails.tags.split(',');
      } else {
        eventDetails.tags = eventDetails.tags;
      }
      eventDetails['createdBy'] = this.localService.getUser().id;
      eventDetails['registeredUsers'] = [];
      eventDetails['configureFields'] = [];
      Object.keys(this.configuredFieldsForm.value).forEach(key => {
        eventDetails.configureFields.push(this.configuredFieldsForm.value[key]);
      });
      this.eventService.saveEvent(eventDetails).subscribe((data: boolean) => {
        if(data) {
          const successMsg = `Event ${eventDetails.name} has been created successfully!`;
          this.notifications.success('Success', successMsg, this.localService.getNotificationsConfig({}));
          this.location.back();
        }
      }, (err: Error) => {
        const errorMsg = err ? err : `Error while creating event ${eventDetails.name}`;
        this.notifications.error('Error', errorMsg, this.localService.getNotificationsConfig({}));
      });
    }
  }

  updateEventDetails(): void {
    if (this.eventForm.valid) {
      let eventDetails = this.eventForm.value;
      if(typeof eventDetails.tags === 'string') {
        eventDetails.tags = eventDetails.tags.split(',');
      } else {
        eventDetails.tags = eventDetails.tags;
      }
      eventDetails['id'] = this.eventDetails.id;
      eventDetails['createdBy'] = this.eventDetails.createdBy;
      eventDetails.registeredUsers = this.eventDetails.registeredUsers;
      eventDetails['configureFields'] = [];
      Object.keys(this.configuredFieldsForm.value).forEach(key => {
        eventDetails.configureFields.push(this.configuredFieldsForm.value[key]);
      });
      this.eventService.updateEvent(eventDetails).subscribe((data: boolean) => {
        if(data) {
          const successMsg = `Event ${eventDetails.name} has been updated successfully!`;
          this.notifications.success('Success', successMsg, this.localService.getNotificationsConfig({}));
          this.router.navigateByUrl(`/dashboard`);
        }
      },
        (err: any) => {
          const errorMsg = err ? err : `Error while updating event ${eventDetails.name}`;
          this.notifications.error('Error', errorMsg, this.localService.getNotificationsConfig({}));
        }
      );
    }
  }

  addConfiguredField() {
    const fieldId = this.attributes.length+1;
    const attr = {
      id: fieldId,
      name: `field${fieldId}`,
      type: 'text',
      mandatory: false,
      value: ''
    };
    const control =  this.fb.group(attr);
    this.configuredFieldsForm.addControl(`field${fieldId}`, control)
    this.attributes.push(attr);
  }

  removeConfiguredField(attr) {
    this.configuredFieldsForm.removeControl(attr.name);
    this.attributes = this.attributes.filter(value => value.id !== attr.id);
  }

  cancel(): void {
    this.location.back();
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.allTags.filter(tag => tag.toLowerCase().indexOf(filterValue) === 0);
  }
}

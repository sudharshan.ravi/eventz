import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCardModule, MatFormFieldModule, MatInputModule,
  MatDatepickerModule, MatSelectModule, MatOptionModule, MatNativeDateModule,
  MatButtonModule, MatTableModule, MAT_CHIPS_DEFAULT_OPTIONS, MatChipsModule,
  MatIconRegistry, MatIconModule, MatAutocompleteModule, MAT_CHECKBOX_CLICK_ACTION,
  MatCheckboxModule, MatDialogModule} from '@angular/material';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { EventRoutingModule, routedComponents } from './event-routing.module';
         
@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    EventRoutingModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatOptionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonModule,
    MatTableModule,
    MatIconModule,
    MatAutocompleteModule,
    MatChipsModule,
    MatCheckboxModule,
    MatDialogModule,
    FontAwesomeModule
  ],
  declarations: [routedComponents],
  providers: [
    MatIconRegistry,
    {
      provide: MAT_CHIPS_DEFAULT_OPTIONS,
      useValue: {
        separatorKeyCodes: [ENTER, COMMA]
      }
    },
    {
      provide: MAT_CHECKBOX_CLICK_ACTION, 
      useValue: 'check'
    }
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class EventModule { }

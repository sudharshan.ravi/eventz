import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { faHome, faCog, faBars } from '@fortawesome/free-solid-svg-icons';
import { NavService, LocalService, NavLink } from './core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class AppComponent {
  faBars = faBars;
  toggleState: boolean = true;
  subscription: any;
  loginSubscription: any;
  links: Array<NavLink>;
  isLoggedIn: boolean = false;
  
  constructor(
    private navService: NavService,
    private router: Router,
    private localService: LocalService
  ) {
    this.generateNavLinks();
    this.subscribeForEvents();
  }

  ngOnInit(): void {
    this.isLoggedIn = this.localService.getLoggedIn();
    if(this.isLoggedIn) {
      this.router.navigate(['/dashboard']);
    } else {
      this.router.navigate(['/login']);
    }
    this.navService.emitLoginNavChangeEvent(this.isLoggedIn);
    setTimeout(() => {
      this.navService.emitLoginNavChangeEvent(this.isLoggedIn);
    }, 10);
  }

  subscribeForEvents(): void{
    this.subscription = this.navService.getSideMenuBarToggleEmitter()
    .subscribe((toggleState) => {
      this.toggleState = toggleState;
    });

    this.loginSubscription = this.navService.getLoginNavChangeEmitter()
    .subscribe((loggedInState) => {
      this.isLoggedIn = loggedInState;
    });
  }

  generateNavLinks(): void {
    this.links = [
      {path: 'dashboard', label: 'Home', icon: faHome},
      {path: '', label: 'Events', icon: faCog, expanded: true, items: [
        {path: '/event/my-events', label: 'My Events'},
        {path: '/event/add', label: 'Create Event'}
      ]},
    ];
  }

  toggleLink(link: NavLink): void {
    if(!this.toggleState) {
      this.toggleState = !this.toggleState;
    }
    link.expanded = !link.expanded;
    this.links.forEach(item => {
      if(link.path === item.path && item.hasOwnProperty('expanded')) {
        item.expanded = link.expanded;
      }
    });
  }

  toggleSideMenuBar(): void {
    this.navService.emitSideMenuBarToggleEvent();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.loginSubscription.unsubscribe();
  }
}

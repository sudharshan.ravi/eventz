import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';
import { Router } from '@angular/router';
import { faTrashAlt, faEdit, faPlus, faCheck } from '@fortawesome/free-solid-svg-icons';

import { EventService, LocalService } from '../core';
import { AppEvent, convertMinsToHrsMins, ConvertCurrencyToINR } from '../core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardComponent implements OnInit {
  faPlus = faPlus;
  faTrashAlt = faTrashAlt;
  faEdit = faEdit;
  faCheck = faCheck;
  convertMinsToHrsMins = convertMinsToHrsMins;
  ConvertCurrencyToINR = ConvertCurrencyToINR;

  userId: number;
  eventDataSource: any = [];
  displayedColumns = ['position', 'name', 'url', 'delete'];
  loading = false;
  clientName: string = '';
  events: Array<AppEvent> = [];

  constructor(
    private eventService: EventService,
    private notifications: NotificationsService,
    private localService: LocalService,
    private router: Router
  ) {
    this.userId = this.localService.getUser().id;
  }

  ngOnInit() {
    this.getEvents();
  }

  getEvents() {
    this.loading = true;
    this.eventService.getEvents()
      .subscribe((data: Array<AppEvent>) => {
        this.loading = false;
        if (data) {
          this.localService.setEvents(data);
          this.events = this.getEventWithStates(data);
        }
      }, (err: Error) => {
          this.loading = false;
          const errorMsg = err ? err : 'Error while Fetching Events';
          this.notifications.error('Error', errorMsg, this.localService.getNotificationsConfig({}));
      });
  }

  getEventWithStates(data: Array<AppEvent>) {
    let events = [];
    data.forEach(event => {
      const user = event.registeredUsers.filter(user => user.id === this.userId);
      if(event.createdBy === this.userId || user.length > 0) {
        event['canRegister'] = false;
      } else {
        event['canRegister'] = true;
      }
      events.push(event);
    });
    return events;
  }

  joinEvent(eventId: string) {
    this.router.navigateByUrl(`/event/${eventId}/register`);
  }

  createEvent() {;
    this.router.navigateByUrl(`/event/add`);
  }
}
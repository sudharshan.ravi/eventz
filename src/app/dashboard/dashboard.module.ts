import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatButtonModule, MatCardModule, MatChipsModule } from '@angular/material';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { routedComponents, DashboardRoutingModule, } from './dashboard-routing.module';

@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    MatCardModule,
    MatChipsModule,
    FontAwesomeModule,
    DashboardRoutingModule
  ],
  declarations: [routedComponents]
})
export class DashboardModule { }

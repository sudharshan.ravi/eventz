
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { faBell, faTimes } from '@fortawesome/free-solid-svg-icons';
import { NotificationsService } from 'angular2-notifications';

import { User, NavService, LocalService, AppNotification, AppNotificationService } from '../../core';
import { ConfirmationDialog } from './confirmation.dialog';

@Component({
  selector: 'app-layout-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class HeaderComponent implements OnInit {
  faBell = faBell;
  faTimes = faTimes;
  navLinks = [];
  toggleState: boolean = true;
  isLoggedIn: boolean = false;
  subscription: any;
  user: User;
  appNotifications: Array<AppNotification> = [];

  constructor(
    private navService: NavService,
    protected router: Router,
    private localService: LocalService,
    private notificationService: AppNotificationService,
    private notifications: NotificationsService,
    public dialog: MatDialog
  ) {
    this.user = this.localService.getUser();
  }

  ngOnInit() {
    this.subscription = this.navService.getSideMenuBarToggleEmitter()
                          .subscribe((toggleState) => {
                            this.toggleState = toggleState;
                          });

    this.subscription = this.navService.getLoginNavChangeEmitter()
                          .subscribe((loggedInState) => {
                            this.isLoggedIn = loggedInState;
                            if(this.isLoggedIn) {
                              this.updateUserMenu();
                              this.getNotifications();
                            }
                          });
  }

  updateUserMenu(): void {
    this.user = this.localService.getUser();
    if(this.user && this.user.lastName) {
      this.user.shortName = this.user.firstName.charAt(0) + this.user.lastName.charAt(0);
      this.user.fullName = `${this.user.firstName} ${this.user.lastName}`;
    } else if(this.user && this.user.fullName) {
      this.user.shortName = this.user.firstName.charAt(0) + this.user.firstName.charAt(1);
      this.user.fullName = this.user.firstName;
    }
  }

  getNotifications(): void {
    this.notificationService.getNotifications(this.user.id)
      .subscribe((data: Array<AppNotification>) => {
        if(data && data.length) {
          this.appNotifications = data.filter(value => value.isRead === false);
        }
      }, (err: Error) => {
        const errorMsg = err ? err : `Error while fetching Notifications`;
        this.notifications.error('Error', errorMsg, this.localService.getNotificationsConfig({}));
      });
  }

  removeNotification(notification: AppNotification): void {
    this.notificationService.removeNotification(notification, this.user.id)
      .subscribe((data: Array<AppNotification>) => {
        if(data && data.length) {
          this.appNotifications = data.filter(value => value.isRead === false);
        }
      }, (err: Error) => {
        const errorMsg = err ? err : `Error while removing Notification: ${notification.message}`;
        this.notifications.error('Error', errorMsg, this.localService.getNotificationsConfig({}));
      });
  }

  logout(): void {
    this.navService.emitLoginNavChangeEvent(false);
    this.localService.setUser(null);
    this.localService.setLoggedIn(false);
    this.router.navigate(['/login']);
  }

  resetApp(): void {
    let dialogRef = this.dialog.open(ConfirmationDialog, {
      disableClose: false
    });
    dialogRef.componentInstance.confirmMessage = `Are you sure you want to Reset Application and Start Again?`;
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        localStorage.removeItem('user');
        localStorage.removeItem('users');
        localStorage.removeItem('notifications');
        localStorage.removeItem('events');
        const successMsg = `Application has been reset. You can Start Again!`;
        this.notifications.success('Success', successMsg, this.localService.getNotificationsConfig({}));
      }
      dialogRef = null;
    });
  }

  home(): void {
    this.router.navigate(['/']);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}

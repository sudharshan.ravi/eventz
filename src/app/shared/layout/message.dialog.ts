import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-message-dialog',
  templateUrl: './message.dialog.html',
  styleUrls: ['./message.dialog.scss'],
  encapsulation: ViewEncapsulation.None
})

export class MessageDialog implements OnInit{
  public messageTitle: string;
  public messages: Array<any>;

  constructor(public dialogRef: MatDialogRef<MessageDialog>) {
    
  }

  ngOnInit() {
    let messages = this.messages;
  }
}
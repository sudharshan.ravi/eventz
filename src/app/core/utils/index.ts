export const ConvertCurrencyToINR = (num: string): string => {
  let n1, n2;
  num = num + '' || '';
  n1 = num.split('.');
  n2 = n1[1] || null;
  n1 = n1[0].replace(/(\d)(?=(\d\d)+\d$)/g, "$1,");   
  num = n2 ? n1 + '.' + n2 : n1;
  n1 = num.split('.');
  n2 = (n1[1]) || null;
  if (n2 !== null) {
           if (n2.length <= 1) {
                   n2 = n2 + '0';
           } else {
                   n2 = n2.substring(0, 2);
           }
   }
   num = n2 ? n1[0] + '.' + n2 : n1[0];

   return '₹' + num;
}

export const convertMinsToHrsMins = (mins: number): string => {
  const h = Math.floor(mins / 60);
  const m = mins % 60;
  if(!h) {
   return `${m}mins`; 
  } else if(!m) {
   return h === 1 ? `${h}hr` : `${h}hrs`;  
  } else {
   return `${h}hrs ${m}mins`;
  }
}
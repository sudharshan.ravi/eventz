import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LocalService, LoginService, EventService, NavService } from './services';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    LoginService,
    EventService,
    LocalService,
    NavService
  ],
  declarations: []
})
export class CoreModule { }

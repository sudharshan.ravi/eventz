export interface AppNotification {
  id: string,
  message: string,
  type: string,
  isRead: boolean
}
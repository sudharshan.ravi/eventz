interface NotificationDetails {
  id: string,
  message: string,
  isRead: boolean
}

export interface User {
  id: number,
  userName: string,
  firstName: string,
  lastName: string,
  email: string,
  password?: string
  shortName?: string,
  fullName?: string,
  notifications?: Array<NotificationDetails>
}
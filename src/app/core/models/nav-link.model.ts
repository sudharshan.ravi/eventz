export interface NavLink {
  path: string;
  label: string;
  icon?: any,
  items?: Array<NavLink>,
  pathMatch?: string,
  expanded?: boolean,
  className?: string
}
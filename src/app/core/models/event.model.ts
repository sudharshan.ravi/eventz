interface EventUserInfo {
  id: number,
  fieldDetails?: Array<ConfiguredField>
}

export interface FieldType {
  type: string,
  name: string
}

export interface ConfiguredField {
  id: number,
  type: string,
  name: string,
  value: string,
  mandatory: boolean
}

export interface AppEvent {
  id: string,
  name: string,
  description: string,
  date: Date,
  duration: number,
  location: string,
  fees: number,
  maxParticipants: number,
  tags: string[],
  createdBy: number,
  canRegister?: boolean,
  configureFields?: Array<ConfiguredField>,
  registeredUsers?: Array<EventUserInfo>
}
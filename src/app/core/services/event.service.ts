import { Injectable } from '@angular/core';

import { DatePipe } from '@angular/common';
import { Observable } from 'rxjs';
import { LocalService } from './local.service';
import { AppEvent } from '../models';
const uuidv4 = require('uuid/v4');

@Injectable()
export class EventService {

  pipe = new DatePipe('en-IN');

  constructor (
    private localService: LocalService
  ){}

  getEvents(): Observable<any> {
    let events = this.localService.getEvents();
    // Get Default Set of Events when no events found on storage
    if(!events) {
      events = this.localService.getDefaultEvents();
    }
    this.localService.setEvents(events);
    if(events.length) {
      return Observable.create((observer) => observer.next(events));
    } else {
      return Observable.create((observer) => observer.next(null));
    }
  }

  getTags(): Observable<any> {
    const tags = this.localService.getTags();
    return Observable.create((observer) => observer.next(tags));
  }

  getEventDetails(eventId: string) {
    if(eventId) {
      const event = this.localService.getEvents()
                      .filter(event => event.id === eventId)[0];
      return Observable.create((observer) => observer.next(event));
    } else {
      return Observable.create((observer) => observer.next(null));
    }
  }

  saveEvent(event: AppEvent): Observable<any> {
    if(event) {
      let events = this.localService.getEvents();
      event.id = uuidv4();
      events.push(event);
      this.localService.setEvents(events);
      return Observable.create((observer) => observer.next(true));
    } else {
      return Observable.create((observer) => observer.next(null));
    }
  }

  updateEvent(event: AppEvent): Observable<any> {
    if(event.id) {
      let events = this.localService.getEvents();
      let notifications = this.localService.getNotifications();
      if(!notifications) {
        notifications = [];
      }
      const notificationInfo = {
        id: event.id, 
        message: `${event.name} has been updated on ${this.pipe.transform(new Date(), 'MMM d, y, h:mm:ss a')}`,
        type: 'event',
        isRead: false
      };
      notifications.push(notificationInfo);
      this.localService.setNotifications(notifications);
      const updateIndex = events.findIndex(value => value.id == event.id);
      events.splice(updateIndex, 1, event);
      this.localService.setEvents(events);
      return Observable.create((observer) => observer.next(true));
    } else {
      return Observable.create((observer) => observer.next(null));
    }
  }

  removeEvent(eventId: string): Observable<any> {
    if(eventId) {
      const events = this.localService.getEvents();
      const event = events.filter(value => value.id === eventId)[0];
      let notifications = this.localService.getNotifications();
      if(!notifications) {
        notifications = [];
      }
      const notificationInfo = {
        id: event.id, 
        message: `${event.name} has been removed on ${this.pipe.transform(new Date(), 'MMM d, y, h:mm a')}`,
        type: 'event',
        isRead: false
      };
      notifications.push(notificationInfo);
      this.localService.setNotifications(notifications);
      const newEvents = events.filter(value => value.id !== eventId);
      this.localService.setEvents(newEvents);
      return Observable.create((observer) => observer.next(true));
    } else {
      return Observable.create((observer) => observer.next(null));
    }
  }

  getMyEvents(userId: number): Observable<any> {
    if(userId) {
      const events = this.localService.getEvents();
      const myEvents = events.filter(value => value.createdBy === userId);
      return Observable.create((observer) => observer.next(myEvents));
    } else {
      return Observable.create((observer) => observer.next(null));
    }
  }

  register(userId: number, event: AppEvent): Observable<any> {
    if(userId) {
      const user = this.localService.getUsers()
                    .filter(user => user.id === userId);
      if(user.length) {
        let events = this.localService.getEvents();
        const updateIndex = events.findIndex(value => value.id == event.id);
        events.splice(updateIndex, 1, event);
        this.localService.setEvents(events);
        return Observable.create((observer) => observer.next(events));
      }
    } else {
      return Observable.create((observer) => observer.next(null));
    }
  }

  getRegisteredUsers(eventId: string): Observable<any> {
    if(eventId) {
      const event = this.localService.getEvents()
                    .filter(event => event.id === eventId);
      const users = this.localService.getUsers();
      const registeredUsers = event[0].registeredUsers;
      let userDetails = [];
      registeredUsers.forEach(value => {
        users.forEach(user => {
          if(value.id === user.id) {
            userDetails.push(user);
          }
        });
      });
      return Observable.create((observer) => observer.next(userDetails));
    } else {
      return Observable.create((observer) => observer.next(null));
    }
  }
}

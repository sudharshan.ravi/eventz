import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { LocalService } from './local.service';

@Injectable()
export class LoginService {
  constructor (
    private localService: LocalService
  ) {}

  login(data) {
    let users = this.localService.getUsers();
    if(!users) {
      users = this.localService.getDefaultUsers();
    }
    this.localService.setUsers(users);
    let user = users.filter(value => value.userName === data.userName && value.password === data.password);
    if(user.length) {
      return Observable.create((observer)=> observer.next(user[0]));
    } else {
      return Observable.create((observer)=> observer.next(null));
    }
  }
}

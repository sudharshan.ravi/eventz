import { Injectable } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';

import { User, AppEvent, AppNotification } from '../index';

@Injectable()
export class LocalService {

  isLoggedIn: boolean = false;

  constructor (
    private notifications: NotificationsService
  ) {}

  getDefaultUsers() {
    return [{id: 1, userName: 'sudharshan', firstName: 'Sudharshan', lastName: 'Ravi', email: 'sudharshan.ravi@gmail.com', password: '!abcd1234', notifications: []},
            {id: 2, userName: 'david', firstName: 'David', lastName: 'Enoch', email: 'david35@gmail.com', password: '!abcd1234', notifications: []},
            {id: 3, userName: 'john', firstName: 'john', lastName: 'paul', email: 'john@gmail.com', password: '!abcd1234', notifications: []},
            {id: 4, userName: 'ram', firstName: 'ram', lastName: 'kumar', email: 'ramkumar@gmail.com', password: '!abcd1234', notifications: []},
            {id: 5, userName: 'kishore', firstName: 'kishore', lastName: 'kumar', email: 'kishorekumar@gmail.com', password: '!abcd1234', notifications: []}];
  }

  getDefaultEvents(): Array<AppEvent> {
    return [{
              id: '14cdc36d-7b14-4806-9fb9-5a742887cad9',
              name: 'Comedy Nights with Kapil', 
              description: 'Non stop comedy entertainment by one and only Kapil Sharma. In this show, Kapil Sharma, the host, starts off with a comedy act and later moves on to interview celebrities with fellow co-actors.', 
              date: new Date('09/15/2018'),
              duration: 45, 
              location: 'Rani Seethai Hall, Chennai', 
              fees: 150,
              tags: ['Comedy'],
              maxParticipants: 30,
              configureFields: [{id: 1, name: 'ID Card Number', type: 'text', value: '', mandatory: true}, {id: 2, name: 'Address', type: 'textarea', value: '', mandatory: true}],
              registeredUsers: [],
              createdBy: 1
            },
            {
              id: '1215eb2f-49df-4eec-8cec-6352d28331d4',
              name: 'Out Of Order', 
              description: 'Out Of Order is a riotous situational comedy written by the master of farces – Ray Cooney! The situations the protagonist, a Junior Minister of the Conservative Party keeps getting into and how he keeps extricating himself from them, form the basis for the play.', 
              date: new Date('09/22/2018'),
              duration: 180, 
              location: 'Museum Theatre, Chennai', 
              fees: 500,
              tags: ['Comedy', 'Drama'],
              maxParticipants: 200,
              configureFields: [{id: 1, name: 'ID Card Number', type: 'text', value: '', mandatory: true}, {id: 2, name: 'Address', type: 'textarea', value: '', mandatory: true}, {id: 3, name: 'Alternate Email ID', type: 'text', value: '', mandatory: false}],
              registeredUsers: [],
              createdBy: 1
            },
            {
              id: '406cc41c-79c8-45da-8ddb-81d918d7c19a',
              name: 'Hybrid Vehicle Workshop', 
              description: 'TOP ENGINEERS is an organization which is run by senior industries, MIT-ANNA UNIVERSITY,  Chennai alumni, renowned subject matter experts and researchers to bring out the real world experience', 
              date: new Date('09/28/2018'),
              duration: 60, 
              location: 'ICSA Program Center, Chennai', 
              fees: 100,
              tags: ['Science', 'Technology'],
              maxParticipants: 150,
              configureFields: [{id: 1, name: 'ID Card Number', type: 'text', value: '', mandatory: true}, {id: 2, name: 'Address', type: 'textarea', value: '', mandatory: true}, {id: 3, name: 'Alternate Email ID', type: 'text', value: '', mandatory: false}],
              registeredUsers: [],
              createdBy: 2
            },
            {
              id: '9f12c44b-f6cf-4cf5-adfe-c58909ecd3e2',
              name: 'Fandom Presents Blackstratblues', 
              description: 'At Fandom, our new live venue, prepare to be blown away by sensational live performances. Our 500 standing capacity venue is fully equipped with a grand stage and a robust sound and lighting system.', 
              date: new Date('10/02/2018'),
              duration: 150, 
              location: 'Fandom, Bengaluru', 
              fees: 200,
              tags: ['Drama'],
              maxParticipants: 500,
              configureFields: [{id: 1, name: 'ID Card Number', type: 'text', value: '', mandatory: true}, {id: 2, name: 'Address', type: 'textarea', value: '', mandatory: true}, {id: 3, name: 'Alternate Email ID', type: 'text', value: '', mandatory: false}],
              registeredUsers: [],
              createdBy: 2
            }];
  }

  getTags(): string[] {
    return ['Comedy', 'Drama', 'Hackathon', 'Techincal Meetup', 'Science', 'Technology', 'Meetup', 'Cricket', 'Football']
  }

  getLoggedIn() {
    return JSON.parse(window.localStorage.getItem('isLoggedIn'));
  }

  setLoggedIn(value: boolean): void {
    window.localStorage.setItem('isLoggedIn', JSON.stringify(value));
  }

  getUsers(): Array<User> {
    return JSON.parse(window.localStorage.getItem('users'));
  }

  setUsers(value: Array<User>): void {
    window.localStorage.setItem('users', JSON.stringify(value));
  }

  getUser(): User {
    return JSON.parse(window.localStorage.getItem('user'));
  }

  setUser(value: User): void {
    window.localStorage.setItem('user', JSON.stringify(value));
  }

  getEvents(): Array<AppEvent> {
    return JSON.parse(window.localStorage.getItem('events'));
  }

  setEvents(events: Array<AppEvent>): void {
    window.localStorage.setItem('events', JSON.stringify(events));
  }

  getNotifications(): Array<AppNotification> {
    return JSON.parse(window.localStorage.getItem('notifications'));
  }

  setNotifications(events: Array<AppNotification>): void {
    window.localStorage.setItem('notifications', JSON.stringify(events));
  }

  getNotificationsConfig(config) {
    return {
      timeOut: config.timeOut ? config.timeOut : 3000,
      showProgressBar: config.showProgressBar ? config.showProgressBar : true,
      pauseOnHover: config.pauseOnHover ? config.pauseOnHover : true,
      clickToClose: config.clickToClose ? config.clickToClose : true,
      clickIconToClose: config.clickIconToClose ? config.clickIconToClose : true,
      maxLength: 1000
    }
  }
}

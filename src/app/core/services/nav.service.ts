import { Injectable, EventEmitter, OnInit } from '@angular/core';
import { LocalService } from './local.service';

@Injectable()
export class NavService implements OnInit {
  loginNavChange: EventEmitter<Object> = new EventEmitter();
  sideMenuBarToggle: EventEmitter<boolean> = new EventEmitter();
  isSideMenuBarOpen = true;
  isLoggedIn = true;

  constructor(
    private localServie: LocalService
  ) {}

  ngOnInit() {
    this.isLoggedIn = this.localServie.getLoggedIn();
  }

  emitLoginNavChangeEvent(value: boolean) {
    this.isLoggedIn = value;
    this.loginNavChange.emit(this.isLoggedIn);
  }

  emitSideMenuBarToggleEvent() {
    this.isSideMenuBarOpen = !this.isSideMenuBarOpen;
    this.sideMenuBarToggle.emit(this.isSideMenuBarOpen);
  }

  getSideMenuBarToggleEmitter() {
    return this.sideMenuBarToggle;
  }

  getLoginNavChangeEmitter() {
    return this.loginNavChange;
  }
}

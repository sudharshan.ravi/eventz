import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { LocalService } from './local.service';
import { AppNotification } from '../models';

@Injectable()
export class AppNotificationService {
  constructor (
    private localService: LocalService
  ){}

  getNotifications(userId: number): Observable<any> {
    let notifications = this.localService.getNotifications();
    if(!notifications) {
      notifications = [];
    }
    this.localService.setNotifications(notifications);
    if(notifications.length) {
      let users = this.localService.getUsers();
      const events = this.localService.getEvents();
      const updateIndex = users.findIndex(value => value.id == userId);
      const nonUserEvents = events.filter(value => { 
              let registeredUser = value.registeredUsers.filter(user => user.id === userId);
              return registeredUser && registeredUser.length ? true : false;
            });
      let user = users[updateIndex];
      notifications.forEach(value => {
        nonUserEvents.forEach(event => {
          if(event.id === value.id) {
            const userNf = user.notifications.filter(notification => notification.id === event.id && notification.message === value.message)[0];
            if(!userNf) {
              user.notifications.push(value);
            }
          }
        })
      });
      users.splice(updateIndex, 1, user);
      this.localService.setUser(user);
      this.localService.setUsers(users);
      return Observable.create((observer) => observer.next(user.notifications));
    } else {
      return Observable.create((observer) => observer.next(null));
    }
  }

  removeNotification(notification: AppNotification, userId: number): Observable<any> {
    if(notification.id) {
      let users = this.localService.getUsers();
      const updateIndex = users.findIndex(value => value.id == userId);
      let user = users[updateIndex];
      user.notifications.forEach(value => { 
        if(value.id === notification.id && value.message === notification.message) {
          value.isRead = true;
        }
      });
      users.splice(updateIndex, 1, user);
      this.localService.setUser(user);
      this.localService.setUsers(users);
      return Observable.create((observer) => observer.next(user.notifications));
    } else {
      return Observable.create((observer) => observer.next(null));
    }
  }
}

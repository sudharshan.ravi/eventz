# Eventz

Event Registration Application

## Prerequisites

Install any one of [NodeJS 8 versions](https://nodejs.org/en/download/)

Run `npm install` to install the App dependencies


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## Events Users

Following are the list of default users who can access this application. 
All of them have the same password `!abcd1234`

`sudharshan`  2 Events assigned by default

`david`  2 Events assigned by default

`john`

`ram`

`kishore`


## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.


## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
